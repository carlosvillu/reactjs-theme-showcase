# Schibsted Spain theme Showcase

## Dependencies

Link dependencie with the unpublish sui-button component
```
$ git clone https://bitbucket.org/workshopacme/sui-button
$ cd sui-button
$ npm link
```

Then in the `reactjs-theme-showcase` folder link the sui-button

```
$ npm link sui-button
```

Maybe after try to link `sui-button` the system entry in a infinite loop. To fix that problem (dirty way):

* Go to sui-button folder.
* Unlink the repo: `$ npm unlink`
* Remove of the `package.json` the entry: ` "postinstall": "npm run build"`
* Build the project again: `$ npm run build`
* Link again: `npm link`
* Go to the `reactjs-theme-showcase`
* Link again the button: `npm link sui-button`
