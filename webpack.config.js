var path = require('path');
var config = {
  entry: {
    index: path.resolve(__dirname, 'src/jsx/index.jsx'),
    autocompleted: path.resolve(__dirname, 'src/jsx/autocompleted.jsx'),
    button: path.resolve(__dirname, 'src/jsx/button.jsx')
  },
  output: {
    path: path.resolve(__dirname, 'public/js'),
    filename: '[name].bundle.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  module: {
    loaders: [{
      test: /\.jsx?$/, // A regexp to test the require path. accepts either js or jsx
      loader: 'babel' // The module to load. "babel" is short for "babel-loader"
    }]
  }
};

module.exports = config;
