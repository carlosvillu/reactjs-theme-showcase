import React from 'react'

import {Autocompleted} from '@schibstedspain/sui-autocompleted';
import AutocompletedContainer from '@schibstedspain/sui-autocompleted/docs/autocompleted-container'
import AutocompletedGithubUserContainer from '@schibstedspain/sui-autocompleted/docs/autocompleted-githubUsers-container'


React.render(<AutocompletedContainer />, document.getElementById('autocompleted-container-languages'));
React.render(<AutocompletedGithubUserContainer />, document.getElementById('autocompleted-container-github'));
